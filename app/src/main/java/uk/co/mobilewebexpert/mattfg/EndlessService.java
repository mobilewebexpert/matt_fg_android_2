package uk.co.mobilewebexpert.mattfg;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import java.util.Timer;
import java.util.TimerTask;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class EndlessService extends Service {

    private final static String TAG = EndlessService.class.getSimpleName();
    private final static int TIMER_DELAY_SECS = 0;
    private final static int TIMER_PERIOD_SECS = 120 - 110;
    
    private PowerManager.WakeLock wakeLock;
    private boolean isServiceStarted;
    
    private Timer timer;
    private TimerTask timerTask;
    public int timerCounter = 0;

    @Nullable
    public IBinder onBind(@NonNull Intent intent) {
        Utils.log("Some component want to bind with the service");
        return null;
    }

    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Utils.log("onStartCommand executed with startId: " + startId);
        if (intent != null) {
            String action = intent.getAction();
            Utils.log("using an intent with action " + action);
            if (Actions.START.name().equals(action)) {
                startService();
            }
            else if (Actions.STOP.name().equals(action)) {
                stopService();
            }
            else {
                Utils.log("This should never happen. No action in the received intent");
            }
        }
        else {
            Utils.log("with a null intent. It has been probably restarted by the system.");
        }

        return START_STICKY;
    }

    public void onCreate() {
        super.onCreate();
        Utils.log("The service has been created");
        Notification notification = createNotification();
        startForeground(1, notification);
    }

    public void onDestroy() {
        super.onDestroy();
        Utils.makeToast(getApplicationContext(), "Service destroyed");
    }

    public void onTaskRemoved(@NonNull Intent rootIntent) {

        Intent restartServiceIntent = new Intent(this.getApplicationContext(), EndlessService.class);
        restartServiceIntent.setPackage(getPackageName());

        //PendingIntent restartServicePendingIntent = PendingIntent.getService(this, 1, restartServiceIntent, 0);
        PendingIntent restartServicePendingIntent = PendingIntent.getService(this, 1, restartServiceIntent, PendingIntent.FLAG_IMMUTABLE);
        getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        AlarmManager alarmManager = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 1000, restartServicePendingIntent);
    }

    private void startService() {
        if (!this.isServiceStarted) {
            Utils.makeToast(getApplicationContext(), "Service starting its task");
            this.isServiceStarted = true;
            ServiceTracker.setServiceState(this, ServiceState.STARTED);

            // we need this lock so our service gets not affected by Doze Mode
            PowerManager powerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);

            wakeLock = powerManager.newWakeLock(1, TAG);
            wakeLock.acquire();

//            while (isServiceStarted) {
//                try {
//                    Thread.sleep(10 * 1000);
//                    pingFakeServer();
//                }
//                catch(InterruptedException ie) {
//                    Utils.log("Sleep interrupted");
//                }
//            }
//            Utils.log("End of the loop for the service");
            startTimer();

        }
    }

    private void stopService() {

        Utils.makeToast(getApplicationContext(), "Service stopping");

        try {
            if (wakeLock != null && wakeLock.isHeld()) {
                wakeLock.release();
            }

            stopForeground(true);
            stopSelf();
        }
        catch (Exception e) {
            Utils.log("Service stopped without being started: " + e.getMessage());
        }

        stopTimerTask();

        isServiceStarted = false;
        ServiceTracker.setServiceState(this, ServiceState.STOPPED);
    }

    private void pingFakeServer() {

        Utils.makeToast(getApplicationContext(), "Ping!");
        Utils.playSound(getApplicationContext(), R.raw.elevator_bell);
    }

    private Notification createNotification() {
        String notificationChannelId = "ENDLESS SERVICE CHANNEL";

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationChannel channel = new NotificationChannel(notificationChannelId, "Endless Service notifications channel", NotificationManager.IMPORTANCE_HIGH);
        channel.setDescription("Endless Service channel");
        channel.enableLights(true);
        channel.setLightColor(Color.RED);
        channel.enableVibration(true);
        channel.setVibrationPattern(new long[] {100L, 200L, 300L, 400L, 500L, 400L, 300L, 200L, 400L});
        notificationManager.createNotificationChannel(channel);

        //PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class),  0);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class),  PendingIntent.FLAG_IMMUTABLE);
        Notification.Builder builder = new Notification.Builder(this, notificationChannelId);
        return builder
                .setContentTitle("Persistent Service")
                .setContentText("Try killing all apps and/or restarting phone.")
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker("Ticker text").build();
    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every x seconds
        timer.schedule(timerTask, TIMER_DELAY_SECS * 1000, TIMER_PERIOD_SECS * 1000);
    }


    public void initializeTimerTask() {

        timerTask = new TimerTask() {

            public void run() {
                
//            Log.d(TAG, "in timer ++++  "+ (timerCounter++));

//            if (timerCounter == 120 - 110) {

                timerCounter = 0;

                //appobj.webService.sendPulseToServer();
                pingFakeServer();

//            }
            }
        };
    }


    public void stopTimerTask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }
}
