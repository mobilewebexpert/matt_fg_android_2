package uk.co.mobilewebexpert.mattfg;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

public class StartReceiver extends BroadcastReceiver {

    public void onReceive(@NonNull Context context, @NonNull Intent intent) {

        if (intent == null) {
            Utils.log("BOOT null intent!");
            Utils.makeToast(context.getApplicationContext(), "BOOT null intent!");
        }
        else {
            Utils.log("BOOT action: " + intent.getAction());
            Utils.makeToast(context.getApplicationContext(), "BOOT action:" + intent.getAction());
        }

        if (("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())
                || "android.intent.action.LOCKED_BOOT_COMPLETED".equals(intent.getAction())
                || "android.intent.action.QUICKBOOT_POWERON".equals(intent.getAction())
                || "android.intent.action.REBOOT".equals(intent.getAction()))
                && ServiceTracker.getServiceState(context) == ServiceState.STARTED) {

            Intent intent2 = new Intent(context, EndlessService.class);
            intent2.setAction(Actions.START.name());
            Utils.log("Starting the service from a BroadcastReceiver");
            context.startForegroundService(intent2);
        }
    }
}