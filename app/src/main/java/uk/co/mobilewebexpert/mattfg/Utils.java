package uk.co.mobilewebexpert.mattfg;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

public class Utils {

    private static final String TAG = Utils.class.getSimpleName();

    public static void log(@NonNull String msg) {
        Log.d(TAG, msg);
    }

    public static void makeToast(Context context, String msg) {

        Log.d(TAG, msg);

        new Handler(Looper.getMainLooper()).post(() ->
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        );
    }

    public static void playSound(Context context, int soundResId) {

        try {
            MediaPlayer mp = MediaPlayer.create(context, soundResId);
            mp.start();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
