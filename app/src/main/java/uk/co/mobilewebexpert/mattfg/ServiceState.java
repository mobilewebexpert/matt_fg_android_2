package uk.co.mobilewebexpert.mattfg;

public enum ServiceState {
    STARTED,
    STOPPED
}