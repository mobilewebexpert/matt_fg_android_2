package uk.co.mobilewebexpert.mattfg;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View btnStartService = findViewById(R.id.btnStartService);
        btnStartService.setOnClickListener((View view) -> {
            Utils.log("START THE FOREGROUND SERVICE ON DEMAND");
            actionOnService(Actions.START);
        });

        View btnStopService = findViewById(R.id.btnStopService);
        btnStopService.setOnClickListener((View view) -> {
            Utils.log("STOP THE FOREGROUND SERVICE ON DEMAND");
            actionOnService(Actions.STOP);
        });
    }

    private void actionOnService(Actions action) {
        if (ServiceTracker.getServiceState(this) == ServiceState.STOPPED && action == Actions.STOP) {
            Utils.log("Service already stopped!");
        }
        else {
            Intent intent = new Intent(this, EndlessService.class);
            intent.setAction(action.name());
            Utils.log("Starting the service");
            startForegroundService(intent);
        }
    }
}